# FrontEnd Test Trovit

> You will need to develop a gallery widget similar to https://codepen.io/nakome/pen/sIcin
>
> Specs:
>
> * The gallery must load images from Flickr's API.
> * The gallery should be able to show the owner's username, which points to the original image post and the image caption.
> * On image click, a responsive lightbox should beopened with all image information that you can include.
> * The gallery should paginate between different image sets.
>
> Important:
>
> * We use React. You can use whatever you want. If it's React you'll have extra points :D
> * Code quality is important for us, so take care of how you structure it and keep an eye on JavaScript best practices.
> * Commit your progress: you can use github, gitlab or bitbucket, simply share the repository with us.
> * Documentation, Unit Testing, Functional Testing if you consider relevant for the exercise.
>
> Extra points:
>
> * Make the gallery responsive.
> * Asynchronous image load treatment.
> * Remember to create a README.md file and describe any decisions you take during the implementation that can help us understand your reasoning.
>
> Note: Please, don’t use create-react-app to the backend, bootstrap to the css or a similar frameworks to precomplete the task, this test is to see your way or code :)

## How to run it

First install dependencies running:

```
yarn
```

Now, just run this command in the project root path:

```
yarn run dev
```

Then in your browser go to `http://localhost:8080/`.

And for the tests run:

```
yarn test
```

## Reasoning

Before I began to work I decided to do a little road map based on the reading of the specs
in order to get my work organized during the develop of this test.

### Initial packages and libraries boilerplate

First of all I've set up all the boilerplate work installing and configuring the libraries
that I assume I'll use for this development.

Since you ask to not use any framework I just took my very own configuration files and library
sets from here and there from some of my GitHub repos to have my base project working. I'm not a big fan of the tooling boilerplate, I enjoy the performance results but I feel this tedious when I have to do it from the scratch (again, and again, and again...).

So this is the toolset I initially thought it's gonna be handy for this exercise:

* `React`: Since we all love it's components, and how our life is less miserable so we can delegate the DOM handling, event delegation and so on.

* `Babel`: To be able to use all the fun of ES6 and beyond.

* `Flow`: I've used it in the past in my own experiments, I'm also using TypeScript in my current job, and having javascript with types is just fantastic, not only for the avoiding type bugs but also because the code is more explicit and self-descriptive.

* `Immutable`: So it provides some cool immutable data-structures.

* `SASS`: To enrich my CSS, I'll see if I add some css-in-js library, but for the moment this will be enough.

* `JEST`: In combination with enzyme for the tests.

* `ESLint`: As code linter, a industry-standard tool with lots of useful plugins, in combination with prettier make your life easier.

* `Webpack`: For the bundling. I had not the change to play with the latest version, so I use it with an old config I know.

I will add more packages probably during the development, but with those I feel very comfortable for the moment (probably with less of them, but less work to do in the future, and I always can remove them).

I also created a little code sample in the `src` folder so I could check if it was working.

### Design layout and components

I normally first do the components working separately form their containers and feature logics, sometimes I develope them directly in storybook, It's a way that helps me to do it the most independent, reusable, and self-contained as possible. I'm not using storybook for this test, but I'm gonna do it in the same fashion, so I have began to do the visual components first, thinking about their needs and the data they will need to do their role.

I began with the `GalleryItem` component, so it's the most obvious and it's visual behaviour as a grid item component within the Gallery container I've created before.

The next I've done is the Modal component from the scratch with some references from the internet, since I don't usually develop this kind of components.

As you will see, I used just `CSS` with `SASS` features, not CSS-in-JS library as `glamurous` or `emotion`. I love this kind of libraries, but it also configuration and more boilerplate. If you know CSS you will have no problem with this fantastic libraries (nor with SASS), I think so at least. I also used BEM, because it's a methodology I like and makes my brain happier when I have to put names and organize class names and so on.

Finally I added some snapshot tests in order to ensure that components and it's behaviour depending on their props are not altered accidentally.

### State management and component behaviour

Now I have the basic components I need to do the gallery with the requirements I want to simulate real data feeding the app and how this data will interact with my components.

To do so I'm gonna use `Redux` as a state manager. Since it isn't imperative, but convenient, to do this application, I want to used in order to show that I, at least, understand the basics and have a expertise using this library.

At first I used a mocked data set from Flickr to avoid do fetching to the API for the moment. It will be enough for do the functionality, after that I could wire the app with the real API.

To deal with async actions, the network request to the Flickr API in this case, I've decided to use `redux-thunk` middleware, which allows you to convert action creator into thunks that you can evaluate 'later', and also chain dispatches. It's the most simple and standard way to deal with asyncronous requests in with `Redux`, and it was more than enough in the use cases I faced. I implemented it before connect my app with real data in order to not do extra work later.

I decided to treat the photos (or pictures) by it's results and also I deal with the selected photo that the user clicks on in order to display the modal. Separated, I deal with the pagination, not for any special reason, but I felt it more organized this way. So I did with the components also.

I just replace te set of photos when I 'fetch' new ones, since the results can't change from one request to the other (I decided to use the 'most recent' images Flickr endpoint, so it's not so 'immutable') I cant perform any memoization or any caching I think, it has no sense to me at least. and also dont save the 'selectedPhoto' as full object, just as an ID that allows me to select the photo from the current page set, so it's less 'memory-consuming' (meh :p).

Those photos, are modeled with a Immutable.js record. I don't know if it's the most suitable case, but I've used it before with good results, so I decided to take advantage of this experience. I thinks it's good to have this kind of models, since I can't do cool thinks as the `fromPayload()` function, direct parse a json payload from the API and do stubs for my tests :)

With the pagination, I keep it simple, is functional, not so fancy but functional. I had to develope a new component, having the last page, the first page, the current page and the next and previous page in any case. I only needed the currentPage and the total pages for that.

### Real data

Since I had a fake data functional app working, I decided to go real. I've got a Flickr API key and began to set up my code.

I didn't go so fancy in this case. I just did a little component, that use `axios` for the fetching, which is a very cool library in this case, more than enough, I could also use the vanilla fetch API, but I didn't feel like I wanted to put time on this, there are a lot of ways to do it.

After that I've done some refinement work on components, in order to fit de spec.

### Final Tests

Last but not least I added tests to reducers, actions(Creators), containers and API.
I probably don't have a 100% coverage, but I don't mind. I don't believe in do test because some people say so, I didn't did test until I understand why test are useful and make me have a better sleep at night, so I like to have the minimum test that I need to have a solid an reliable code or when a bug is introduced and fixed.

I also don't use to do the test at the same time I code, It's because I haven't learnt to code like this, and it's not natural for me. Sometimes I do, sometime I don't. Despite of that I do an effort to do it sometimes, because I think is good for me also.

Also, some functional manual test have been done, it's not a big deal with this app.

#### Disclaimer

I'm not responsible of the pics that this gallery will display if you try to run it (I saw some crazy s\*\*\*t during my tests).

### Further steps

Finally I would like to comment some of the things I would do just for fun (overengineering most of them) if I had unlimited time :)

* As I said, I like CSS-in-JS libraries, I like the idea to have self-contained components that can work and had it's own styles. More reusability (with some cool patterns help) and easier to maintain (since you are not scattering code).

* Also I will make an storybook of those components, a very cool way to develop components, and a very cool way to collect them and show them to product and stakeholders of the company,also as a documentation for the team ("Have we this component already?").

* The async treatment of the images would be a nice thing to have, but I've no experience, neither the time, so I decide to put it apart :(

* Some error handling would be nice to have (I didn't check all the corner cases probably).

* I would like to use some cool libraries for `redux`, like `reselect` or `redux-router-redux`, but I though it has no sense in this app, so... I didn't implemented.

* I also like to use `faker` in my test. Random data in tests is very useful, it's a way to discover new bugs you didn't notices.

* Using service workers for caching data is cool also :) But definitely overengineering. Same happen with code-splitting, we just have one route, I mean...

* I would like to have a "build" working version also.

Probably I'm missing something, but it all that came to my mind by now :)

Any feedback will be much appreciated!