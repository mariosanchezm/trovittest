// @flow

import { List } from 'immutable';
import Photo from '../models/photo';
import { getRecentPhotos } from '../api';

export const REQUEST_PHOTOS = 'REQUEST_PHOTOS';
export const RECEIVE_PHOTOS = 'RECEIVE_PHOTOS';
export const SET_SELECTED_PHOTO = 'SET_SELECTED_PHOTO';
export const UPDATE_PAGINATION = 'UPDATE_PAGINATION';

export function requestPhotos() {
  return { type: REQUEST_PHOTOS, payload: { isFetching: true } };
}

export function receivePhotos(result: Object) {
  const photos = result.photo.map(photo => Photo.fromPayload(photo));
  return { type: RECEIVE_PHOTOS, payload: { isFetching: false, photos: List(photos) } };
}

export function updatePagination(result) {
  return {
    type: UPDATE_PAGINATION,
    payload: {
      currentPage: result.page,
      pages: result.pages,
      perPage: result.perpage,
      totalItems: result.total
    }
  };
}

export function fetchPhotos(page: number = 1) {
  return (dispatch: Function) => {
    dispatch(requestPhotos());
    return getRecentPhotos(page)
      .then(response => {
        const {
          data: { photos }
        } = response;
        dispatch(receivePhotos(photos));
        dispatch(updatePagination(photos));
        return photos;
      })
      .catch(error => {
        throw Error(`API Request Error: ${error}`);
      });
  };
}

export function setSelectedPhoto(id: number | null) {
  return { type: SET_SELECTED_PHOTO, payload: { selectedId: id } };
}
