import * as actions from '.';
import * as api from '../api';
import * as photoPayload from '../test/stubs/photoPayloadStub.json';
import store from '../store';

const {
  data: { photos }
} = photoPayload;

describe('actions', () => {
  test('requestPhotos', () => {
    expect(actions.requestPhotos()).toMatchSnapshot();
  });

  test('receivePhotos', () => {
    expect(actions.receivePhotos(photos)).toMatchSnapshot();
  });

  test('updatePagination', () => {
    expect(actions.updatePagination(photos)).toMatchSnapshot();
  });

  test('setSelectedPhoto number', () => {
    expect(actions.setSelectedPhoto(1)).toMatchSnapshot();
  });

  test('setSelectedPhoto null', () => {
    expect(actions.setSelectedPhoto(null)).toMatchSnapshot();
  });

  test('fetchPhotos', done => {
    const page = 1;
    const dispatchMock = jest.fn();
    api.getRecentPhotos = jest.fn().mockImplementation(() => Promise.resolve(photoPayload));

    actions
      .fetchPhotos(page)(dispatchMock)
      .then(data => {
        expect(data).toEqual(photos);
        expect(dispatchMock).toHaveBeenCalledWith(actions.requestPhotos());
        expect(dispatchMock).toHaveBeenCalledWith(actions.receivePhotos(photos));
        expect(dispatchMock).toHaveBeenCalledWith(actions.updatePagination(photos));
        expect(api.getRecentPhotos).toHaveBeenCalledTimes(1);
        expect(api.getRecentPhotos).toHaveBeenCalledWith(page);
        done();
      });
  });
});
