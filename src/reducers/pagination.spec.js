import { Map } from 'immutable';
import reducers from './pagination';

describe('UPDATE_PAGINATION', () => {
  it(null, () => {
    const state = reducers(Map({ currentPage: null, pages: null, perPage: null, totalItems: null }), {
      type: 'UPDATE_PAGINATION',
      payload: { currentPage: 2, pages: 84, perPage: 12, totalItems: 1000 }
    });
    expect(state).toEqual(Map({ currentPage: 2, pages: 84, perPage: 12, totalItems: 1000 }));
  });
});
