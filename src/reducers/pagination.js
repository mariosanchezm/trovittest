// @flow

import { Map } from 'immutable';
import { UPDATE_PAGINATION } from '../actions';

const pagination = (
  state: Map<string, any> = Map({
    currentPage: null,
    pages: null,
    perPage: null,
    totalItems: null
  }),
  action: {
    type: string,
    payload: {
      currentPage: number,
      pages: number,
      perPage: number,
      totalItems: number
    }
  }
) => {
  switch (action.type) {
    case UPDATE_PAGINATION:
      return state.merge(action.payload);
    default:
      return state;
  }
};

export default pagination;
