import { Map, List } from 'immutable';
import reducers from './photos';

describe('REQUEST_PHOTOS', () => {
  it(null, () => {
    const state = reducers(Map({ isFetching: false, selectedId: false, results: [] }), {
      type: 'REQUEST_PHOTOS',
      payload: { isFetching: true }
    });
    expect(state).toEqual(Map({ isFetching: true, selectedId: false, results: [] }));
  });
});

describe('RECEIVE_PHOTOS', () => {
  it(null, () => {
    const state = reducers(Map({ isFetching: true, selectedId: false, results: [] }), {
      type: 'RECEIVE_PHOTOS',
      payload: {
        isFetching: false,
        photos: List([
          Map({
            id: '27656459318',
            title: "Cascades d'Orgon",
            ownerId: '89339468@N03',
            ownerName: 'malo 201004',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/89339468@N03/27656459318',
            date: '2018-04-17 15:02:02',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/935/27656459318_abd66f5d8d.jpg'
          })
        ])
      }
    });
    expect(state).toEqual(
      Map({
        isFetching: false,
        selectedId: false,
        results: List([
          Map({
            id: '27656459318',
            title: "Cascades d'Orgon",
            ownerId: '89339468@N03',
            ownerName: 'malo 201004',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/89339468@N03/27656459318',
            date: '2018-04-17 15:02:02',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/935/27656459318_abd66f5d8d.jpg'
          })
        ])
      })
    );
  });
});

describe('SET_SELECTED_PHOTO', () => {
  it(null, () => {
    const state = reducers(
      Map({
        isFetching: false,
        selectedId: false,
        results: [
          {
            id: '26657352677',
            title: '2018-04-17_06-51-50',
            ownerId: '149721825@N08',
            ownerName: 'chicograsel',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/149721825@N08/26657352677',
            date: '2018-04-03 09:39:03',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/810/26657352677_0e39f7813e.jpg'
          }
        ]
      }),
      { type: 'SET_SELECTED_PHOTO', payload: { selectedId: '26657352677' } }
    );
    expect(state).toEqual(
      Map({
        isFetching: false,
        selectedId: '26657352677',
        results: [
          {
            id: '26657352677',
            title: '2018-04-17_06-51-50',
            ownerId: '149721825@N08',
            ownerName: 'chicograsel',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/149721825@N08/26657352677',
            date: '2018-04-03 09:39:03',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/810/26657352677_0e39f7813e.jpg'
          }
        ]
      })
    );
  });
});
describe('SET_SELECTED_PHOTO null', () => {
  it(null, () => {
    const state = reducers(
      Map({
        isFetching: false,
        selectedId: '26657352677',
        results: [
          {
            id: '26657352677',
            title: '2018-04-17_06-51-50',
            ownerId: '149721825@N08',
            ownerName: 'chicograsel',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/149721825@N08/26657352677',
            date: '2018-04-03 09:39:03',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/810/26657352677_0e39f7813e.jpg'
          }
        ]
      }),
      { type: 'SET_SELECTED_PHOTO', payload: { selectedId: null } }
    );
    expect(state).toEqual(
      Map({
        isFetching: false,
        selectedId: null,
        results: [
          {
            id: '26657352677',
            title: '2018-04-17_06-51-50',
            ownerId: '149721825@N08',
            ownerName: 'chicograsel',
            description: '',
            flickrUrl: 'https://www.flickr.com/photos/149721825@N08/26657352677',
            date: '2018-04-03 09:39:03',
            views: '0',
            thumb: 'https://farm1.staticflickr.com/810/26657352677_0e39f7813e.jpg'
          }
        ]
      })
    );
  });
});
