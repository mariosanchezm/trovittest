// @flow

import { Map, List } from 'immutable';
import { REQUEST_PHOTOS, RECEIVE_PHOTOS, SET_SELECTED_PHOTO } from '../actions';
import Photo from '../models/photo';

const photos = (
  state: Map<string, any> = Map({
    isFetching: false,
    selectedId: false,
    results: List()
  }),
  action: {
    type: string,
    payload: {
      isFetching?: boolean,
      selectedId?: number | null,
      photos?: List<Photo>
    }
  }
) => {
  switch (action.type) {
    case REQUEST_PHOTOS:
      return state.set('isFetching', true);
    case RECEIVE_PHOTOS:
      return state.merge({
        isFetching: false,
        results: action.payload.photos
      });
    case SET_SELECTED_PHOTO:
      return state.set('selectedId', action.payload.selectedId);
    default:
      return state;
  }
};

export default photos;
