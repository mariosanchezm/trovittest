// @flow

import { combineReducers } from 'redux';
import photos from './photos';
import pagination from './pagination';

export default combineReducers({
  photos,
  pagination,
});
