// @flow
/* eslint-disable no-underscore-dangle */

const { Record } = require('immutable');

const PhotoRecord = Record({
  id: undefined,
  title: undefined,
  ownerId: undefined,
  ownerName: undefined,
  description: undefined,
  flickrUrl: undefined,
  date: undefined,
  views: undefined,
  thumb: undefined
});

class Photo extends PhotoRecord {
  id: number;
  title: string;
  ownerId: string;
  ownerName: string;
  description: string;
  flickrUrl: string;
  date: string;
  views: number;
  thumb: string;

  constructor(
    id: number,
    title: string,
    ownerId: string,
    ownerName: string,
    description: string,
    flickrUrl: string,
    date: string,
    views: string,
    thumb: string
  ) {
    super({
      id,
      title,
      ownerId,
      ownerName,
      description,
      flickrUrl,
      date,
      views,
      thumb
    });
  }
}

Photo.fromPayload = (payload: Object): Photo =>
  new Photo(
    payload.id,
    payload.title,
    payload.owner,
    payload.ownername,
    payload.description._content.replace(/<[^>]*>/g, ''),
    `https://www.flickr.com/photos/${payload.owner}/${payload.id}`,
    payload.datetaken,
    payload.views,
    payload.url_m
  );
export default Photo;
