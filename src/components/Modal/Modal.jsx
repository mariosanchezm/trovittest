// @flow
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import * as React from 'react';

const Modal = ({ children, onClose }: { children: React.Node, onClose: () => any }) => (
  <div
    className="Modal"
    onClick={event => {
      if (event.target === event.currentTarget) {
        onClose();
      }
    }}
  >
    <div className="Modal__content-wrapper">{children}</div>
    <button className="Modal__close-button" onClick={onClose} />
  </div>
);

export default Modal;
