import React from 'react';
import { shallow } from 'enzyme';
import GalleryItem from './GalleryItem';
import photoStub from '../../test/stubs/photoStub';

describe('GalleryItem', () => {
  it('should render correctly', () => {
    const component = shallow(<GalleryItem photo={photoStub()} />);
    expect(component).toMatchSnapshot();
  });
});
