// @flow
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';
import Photo from '../../models/photo';

const GalleryItem = ({ photo, onClickPhoto }: { photo: Photo, onClickPhoto: (id: number) => any }) => {
  const imageStyle = { backgroundImage: `url(${photo.thumb})` };

  return (
    <article className="GalleryItem">
      <div className="GalleryItem__image" style={imageStyle} onClick={() => onClickPhoto(photo.id)} />
      <div className="GalleryItem__owner">
        <a href={photo.flickrUrl} target="_blank">{photo.ownerName}</a>
      </div>
    </article>
  );
};

export default GalleryItem;
