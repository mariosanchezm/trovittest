// @flow

import React from 'react';
import { Link } from 'react-router-dom';

const Paginator = ({ currentPage, pages }: { currentPage: number, pages: number }) => {
  const FIRST_PAGE = 1;
  const previousPage = currentPage > FIRST_PAGE + 1 ? currentPage - 1 : null;
  const nextPage = currentPage < pages - 1 ? currentPage + 1 : null;

  return (
    <div className="Paginator">
      <Link to={`/${FIRST_PAGE}`} className={FIRST_PAGE === currentPage ? 'Paginator__current_page' : ''}>
        {FIRST_PAGE}
      </Link>
      {previousPage && <Link to={`/${previousPage}`}>{previousPage}</Link>}
      {currentPage !== FIRST_PAGE &&
        currentPage !== pages && (
          <Link to={`/${currentPage}`} className="Paginator__current_page">
            {currentPage}
          </Link>
        )}
      {nextPage && <Link to={`/${nextPage}`}>{nextPage}</Link>}
      <Link to={`/${pages}`} className={pages === currentPage ? 'Paginator__current_page' : ''}>
        {pages}
      </Link>
    </div>
  );
};

export default Paginator;
