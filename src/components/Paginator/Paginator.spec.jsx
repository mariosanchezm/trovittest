import React from 'react';
import { shallow } from 'enzyme';
import Paginator from './Paginator';

describe('Paginator', () => {
  it('should render correctly on first page', () => {
    const component = shallow(<Paginator currentPage={1} pages={10} _ />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly on middle page', () => {
    const component = shallow(<Paginator currentPage={5} pages={10} _ />);
    expect(component).toMatchSnapshot();
  });
  it('should render correctly on last page', () => {
    const component = shallow(<Paginator currentPage={10} pages={10} _ />);
    expect(component).toMatchSnapshot();
  });
});
