// @flow

import axios from 'axios';

const API_KEY = '9f4c16459c5741b127d3318b327905f1';
const DEFAULT_PER_PAGE_PHOTOS = 12;

export const getRecentPhotos = (page: number, perPage: number = DEFAULT_PER_PAGE_PHOTOS) =>
  axios.get(
    `https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=${API_KEY}&extras=description%2C+date_upload%2C+date_taken%2C+owner_name%2C+views%2C+url_m&per_page=${perPage}&page=${page}&format=json&nojsoncallback=1`
  );

export default {
  getRecentPhotos
};
