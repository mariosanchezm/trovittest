import moxios from 'moxios';
import { getRecentPhotos } from '.';
import * as photoPayload from '../test/stubs/photoPayloadStub.json';

test('getAPIData', done => {
  moxios.withMock(() => {
    getRecentPhotos(1);
  });
  moxios.wait(() => {
    const request = moxios.requests.mostRecent();
    request
      .respondWith({
        status: 200,
        response: photoPayload
      })
      .then(() => {
        expect(request.url).toEqual(
          `https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=9f4c16459c5741b127d3318b327905f1&extras=description%2C+date_upload%2C+date_taken%2C+owner_name%2C+views%2C+url_m&per_page=12&page=1&format=json&nojsoncallback=1`
        );
        done();
      });
  });
});
