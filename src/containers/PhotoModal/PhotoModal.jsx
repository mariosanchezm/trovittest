// @flow

import React from 'react';
import { connect } from 'react-redux';
import Modal from '../../components/Modal/Modal';
import Photo from '../../models/photo';
import { setSelectedPhoto } from '../../actions';

const MAX_DESCRIPTION_LENGTH = 200;

const PhotoModal = ({ photo, onClose }: { photo: Photo, onClose: () => any }) => (
  <Modal disabled={!photo} onClose={onClose}>
    <div className="PhotoModal__image">
      <img src={photo.thumb} alt={photo.title} />
    </div>
    <div className="PhotoModal__info">
      <h3>{photo.title}</h3>
      <a href={photo.flickrUrl} target="_blank">
        {photo.ownerName}
      </a>
      <p>{photo.date}</p>
      <p>views: {photo.views}</p>
      {photo.description && (
        <p>
          {photo.description.length > MAX_DESCRIPTION_LENGTH
            ? `${photo.description.substring(0, MAX_DESCRIPTION_LENGTH)}...`
            : photo.description}
        </p>
      )}
    </div>
  </Modal>
);

const mapDispatchToProps = (dispatch: Function) => ({
  onClose: () => dispatch(setSelectedPhoto(null))
});

export default connect(null, mapDispatchToProps)(PhotoModal);
