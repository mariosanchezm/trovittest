import React from 'react';
import { render } from 'enzyme';
import PhotoModal from './PhotoModal';
import photoStub from '../../test/stubs/photoStub';
import storeWrapper from '../../test/tools/storeWrapper';

describe('PhotoModal', () => {
  it('should render correctly', () => {
    const component = render(storeWrapper(<PhotoModal photo={photoStub()} />));
    expect(component).toMatchSnapshot();
  });
});
