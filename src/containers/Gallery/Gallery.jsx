// @flow

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { List } from 'immutable';
import GalleryItem from '../../components/GalleryItem/GalleryItem';
import Paginator from '../../components/Paginator/Paginator';
import { fetchPhotos, setSelectedPhoto } from '../../actions';
import Photo from '../../models/photo';
import PhotoModal from '../PhotoModal/PhotoModal';

class Gallery extends React.Component<{
  match: {
    params: {
      page: number
    }
  },
  photos: List<Photo>,
  selectedPhoto: Photo,
  currentPage: number,
  pages: number,
  fetchPhotos: (page?: number) => any,
  onClickPhoto: (id: number) => any
}> {
  componentWillMount() {
    this.props.fetchPhotos(this.props.match.params.page);
  }

  componentWillUpdate(nextProps) {
    if (this.props.match.params.page !== nextProps.match.params.page) {
      this.props.fetchPhotos(nextProps.match.params.page);
    }
  }

  render() {
    const { photos, selectedPhoto, currentPage, pages, onClickPhoto } = this.props;

    return (
      <div className="Gallery">
        {photos && photos.map(photo => <GalleryItem key={photo.id} photo={photo} onClickPhoto={onClickPhoto} />)}
        {selectedPhoto && <PhotoModal photo={selectedPhoto} />}
        <Paginator currentPage={currentPage} pages={pages} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const photos = state.photos.get('results');
  const selectedPhotoId = state.photos.get('selectedId');
  const currentPage = state.pagination.get('currentPage');
  const pages = state.pagination.get('pages');
  return {
    photos,
    selectedPhoto: photos.find(photo => photo.id === selectedPhotoId),
    currentPage,
    pages
  };
};

const mapDispatchToProps = (dispatch: Function) => ({
  fetchPhotos: (page: number) => dispatch(fetchPhotos(page)),
  onClickPhoto: (id: number) => {
    dispatch(setSelectedPhoto(id));
  }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Gallery));
