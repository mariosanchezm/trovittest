import React from 'react';
import { render } from 'enzyme';
import Gallery from './Gallery';
import { photoStubList } from '../../test/stubs/photoStub';
import storeWrapper from '../../test/tools/storeWrapper';

describe('Gallery', () => {
  it('should render correctly', () => {
    const component = render(
      storeWrapper(<Gallery photo={photoStubList(12)} selectedPhoto={null} currentPage={1} pages={10} />)
    );
    expect(component).toMatchSnapshot();
  });
  it('should render correctly with selected photo', () => {
    const component = render(
      storeWrapper(<Gallery photo={photoStubList(12)} selectedPhoto={1} currentPage={1} pages={10} />)
    );
    expect(component).toMatchSnapshot();
  });
});
