// @flow

import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Gallery from './containers/Gallery/Gallery';
import './styles.scss';

const FourOhFour = () => <h1>404</h1>;

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div className="app">
        <Switch>
          <Route path="/:page?" component={props => <Gallery {...props} />} />
          <Route component={FourOhFour} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
);

export default App;
