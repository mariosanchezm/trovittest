// @flow

import Photo from '../../models/photo';

const photoStub = (id?: string = '26598634217'): Photo =>
  Photo.fromPayload({
    id,
    owner: '144580423@N08',
    secret: 'ea6d676aff',
    server: '891',
    farm: 1,
    title: 'IMG20180412110916',
    ispublic: 1,
    isfriend: 0,
    isfamily: 0,
    license: 0,
    description: { _content: '' },
    dateupload: '1523793388',
    lastupdate: '1523793390',
    datetaken: '2018-04-12 11:09:16',
    datetakengranularity: 0,
    datetakenunknown: 0,
    ownername: 'vinhnguyen29',
    iconserver: '8859',
    iconfarm: 9,
    views: 0,
    tags: '',
    machine_tags: '',
    originalsecret: '9df31586f9',
    originalformat: 'jpg',
    latitude: 0,
    longitude: 0,
    accuracy: 0,
    context: 0,
    media: 'photo',
    media_status: 'ready',
    url_sq: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_s.jpg',
    height_sq: 75,
    width_sq: 75,
    url_t: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_t.jpg',
    height_t: 75,
    width_t: 100,
    url_s: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_m.jpg',
    height_s: '180',
    width_s: '240',
    url_q: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_q.jpg',
    height_q: '150',
    width_q: '150',
    url_m: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff.jpg',
    height_m: '375',
    width_m: '500',
    url_n: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_n.jpg',
    height_n: '240',
    width_n: '320',
    url_z: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_z.jpg',
    height_z: '480',
    width_z: '640',
    url_c: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_c.jpg',
    height_c: '600',
    width_c: '800',
    url_l: 'https://farm1.staticflickr.com/891/26598634217_ea6d676aff_b.jpg',
    height_l: '768',
    width_l: '1024',
    url_o: 'https://farm1.staticflickr.com/891/26598634217_9df31586f9_o.jpg',
    height_o: '6240',
    width_o: '8320',
    pathalias: ''
  });

export const photoStubList = (length: number) => {
  Array.from({ length }).map((element, idx) => photoStub(`${idx}`));
};

export default photoStub;
