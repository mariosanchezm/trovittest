// @flow

import React from 'react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import store from '../../store';

export default (element: React.ElementType) => (
  <Provider store={store}>
    <MemoryRouter>{element}</MemoryRouter>
  </Provider>
);
